/*
 * Copyright (c) 2022.
 * Automation Framework Selenium - Anh Tester
 */

package anhtester.com.mail;

import static anhtester.com.constants.FrameworkConstants.REPORT_TITLE;

/**
 * Data for Sending email after execution
 */
public class EmailConfig {

    //Nhớ tạo mật khẩu app (App Password) cho Gmail mới gửi được nhen
    //Nếu dùng mail của Hosting thì bình thường
    //Enable Override Report and Send mail in config file => src/test/resources/config/config.properties
    //OVERRIDE_REPORTS=yes
    //send_email_to_users=yes

    public static final String SERVER = "mail.anhtester.com";
    public static final String PORT = "587";

    public static final String FROM = "support@anhtester.com";
    public static final String PASSWORD = "J^-Jeep@RN-y";

    public static final String[] TO = {"support@anhtester.com"};
    public static final String SUBJECT = REPORT_TITLE;
}